package com.sda.media;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**  Definim o aplicatie care sa fie un catalog pt diferite tipuri de media(video, music, carti )
 *   Video: nume, durata, gen (comedie, actiune, drama, ) - ranking
 *   Music: artist, nume, durata, ranking.
 *   Carti: autor, nume, numar de pagini, ranking.
 *   Constraint: nume unice (altfel error message);
 *   Utilizatorii pot adauga comentarii la obiecte (salvate in fisiere .txt)
 *   Utilizatorii pot cauta bazat pe nume.
 *   Utilizatorii pot vedea comentarii pt fiecatre tip de media in parte
 *   Acest ranking trebuie calculat in functie de notele date.
 *   Userul sa poata vedea toate obiectele media dintr-o anumita categ. ( video / music / carti )
 */


public class Main {

    public static void main(String[] args) {
        MediaPlatform mediaPlatform = new MediaPlatform();


        Movie video = new Movie("Harry Potter", Duration.ofMinutes(150), VideoType.ACTION, VideoQuality.HD);
//        System.out.println(video);
        video.setRanking(5);
        video.setRanking(4);
        video.setRanking(3.5);
//        System.out.println(video);




        video.addComments("Cea mai tare din parcare");
        video.addComments("E de 5 stele");
//        video.displayComments();
//        System.out.println("===================================================================");

        Music music = new Music("Jon Bon Jovi", "Always", Duration.ofMinutes(5));
//        System.out.println(music);
        music.addComments("Piesa e foarte tare");
//        music.displayComments();



        Book book = new Book("George Cosbuc", "Poezii", 125);
        Book book1 = new Book("Robert Kiosaky", "Ed financiara", 150);
        Book book2 = new Book("Robert Kfsdfds", "Ed financiara1", 150);
//        System.out.println(book);

        mediaPlatform.addMedia(video);
        mediaPlatform.addMedia(music);
        mediaPlatform.addMedia(book);
        mediaPlatform.addMedia(book1);
        mediaPlatform.addMedia(book2);

        mediaPlatform.searchAndDisplay("Harry Potter");
        mediaPlatform.searchAndDisplay("Poezii");

        Movie video2 = new Movie("Harry Potter2", Duration.ofMinutes(150), VideoType.ACTION, VideoQuality.FHD);

        mediaPlatform.addMedia(video2);

        mediaPlatform.getMediaByType(Movie.class);
        mediaPlatform.getMediaByType(Music.class);

        mediaPlatform.searchByAuthor("Robert");

        YoutubeVideo video3 = new YoutubeVideo("Test", Duration.ofMinutes(5),112, VideoQuality.FHD);
//        video3.play();
//        music.play();
//        book2.read();

        List<Playable> playableOjects = new ArrayList<>();
        playableOjects.add(video3);
        playableOjects.add(video2);
        playableOjects.add(music);

        for(Playable playable: playableOjects){
            playable.play();
        }

    }
}
