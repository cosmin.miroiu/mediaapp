package com.sda.media;

import java.time.Duration;

public class NetflixVideo extends Video {
    public NetflixVideo(String name, Duration length, VideoQuality videoQuality) {
        super(name, length, videoQuality);
    }

    @Override
    public void play() {
        System.out.println("Playing netflix video...");
    }
}
