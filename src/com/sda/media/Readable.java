package com.sda.media;

public interface Readable {
    void read();
}
