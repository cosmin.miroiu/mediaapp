package com.sda.media;

import java.time.Duration;

public class Book extends Media implements Readable{
    String authorName;
    int pagesNumber;

    public Book(String authorName, String name, int pagesNumber) {
        super(name);

        this.authorName = authorName;
        this.pagesNumber = pagesNumber;
    }

    public String getAuthorName() {
        return authorName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "authorName='" + authorName + '\'' +
                "Bookname: " + getName() +
                "Ranking: " + getRanking() +
                ", pagesNumber=" + pagesNumber +
                '}';
    }

    @Override
    public void read() {
        System.out.println("Reading " + this);
    }
}
