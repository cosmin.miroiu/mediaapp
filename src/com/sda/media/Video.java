package com.sda.media;

import java.time.Duration;

public abstract class Video extends MediaLength implements Playable{
    private VideoQuality videoQuality;

    public Video(String name, Duration length, VideoQuality videoQuality) {
        super(name, length);
        this.videoQuality = videoQuality;
    }

    @Override
    public String toString() {
        return "Video{" +
                "videoQuality=" + videoQuality +
                super.toString() +
                '}';
    }

}
