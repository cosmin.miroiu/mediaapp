package com.sda.media;

import java.time.Duration;

public class YoutubeVideo extends Video implements Playable{
    private int views;

    public YoutubeVideo(String name, Duration length, int views, VideoQuality videoQuality) {
        super(name, length, videoQuality);
        this.views = views;
    }

    @Override
    public String toString() {
        return "YoutubeVideo{" +
                "views=" + views +
                super.toString() +
                '}';
    }

    @Override
    public void play() {
        System.out.println("Playing youtube video: " + this);
    }
}
