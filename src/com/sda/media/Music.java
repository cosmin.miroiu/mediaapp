package com.sda.media;

import java.time.Duration;

public class Music extends MediaLength implements Playable{

    String artistName;

    public Music(String artistName, String name, Duration length) {
        super(name, length);
        this.artistName = artistName;
    }

    @Override
    public String toString() {
        return "Music{" +
                "artistName='" + artistName + '\'' +
                super.toString() +
                '}';
    }

    @Override
    public void play() {
        System.out.println("Playing track: " + this);
    }
}
