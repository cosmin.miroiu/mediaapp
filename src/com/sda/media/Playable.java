package com.sda.media;

public interface Playable {
    void play();
}
