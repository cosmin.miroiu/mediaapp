package com.sda.media;

import java.time.Duration;

public class Movie extends Video{

    VideoType type;

    public Movie(String name, Duration length, VideoType type, VideoQuality videoQuality) {
        super(name, length, videoQuality);

        this.type = type;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "type=" + type +
                super.toString() +
                '}';
    }

    @Override
    public void play() {
        System.out.println("Playing movie: " + this);
    }
}
